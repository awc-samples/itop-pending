#!/usr/bin/env php
<?php
/*
Report iTop pending jobs via email

Requires pear mail
# pear install Mail

Make executable
# chmod +x itop-pending.php

Run
# ./itop-pending.php
*/

// for email
include 'Mail.php';
include 'Mail/mime.php';

$dated = date("Y_m_d");

// path to report with trailing "/"
$report_path = '/home/ubuntu/report-csv/';

$file_name = 'report-' . $dated . '.csv';
$file_path = $report_path . $file_name;

$send_via_email = true;

// output headers so that the file is downloaded rather than displayed
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=report.csv');

// create a file pointer connected to the output stream
$output = fopen($file_path, 'w');

// output the column headings
fputcsv($output, array('reference', 'name', 'start_date', 'title'));

// fetch the data
mysql_connect('localhost', 'itop_db_user', 'password');
mysql_select_db('itop_db');

$rows = mysql_query('SELECT it.ref,ic.name,it.start_date,it.title
                     FROM itop_ticket it,itop_ticket_request iq,itop_contact ic
                     WHERE iq.id=it.id and it.agent_id=ic.id and iq.status="pending"
                     ORDER BY ic.name,it.start_date');

// loop over the rows, outputting them
while ($row = mysql_fetch_assoc($rows)) fputcsv($output, $row);

// send CSV file via email
if ($send_via_email == true) {
    $to = 'manager@communitytraininginitiatives.com.au';
    $from = 'itop';
    $reply_to = 'noreply@ctinitiatives.com.au';
    $subject = 'CSV report - current pending jobs';

    $text = 'CSV report - current pending jobs';

    $message = new Mail_mime();
        $message->setHTMLBody($text);
        $message->addAttachment($file_path);
        $body = $message->get();
        $extraheaders = array("From"=>$from, "Subject"=>$subject,"Reply-To"=>$reply_to);
        $headers = $message->headers($extraheaders);
        $mail = Mail::factory("mail");
        $mail->send($to, $headers, $body);
}
?>
